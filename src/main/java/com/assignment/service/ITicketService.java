package com.assignment.service;

import java.util.List;

import com.assignment.model.TicketRequest;
import com.assignment.model.TicketResponse;

public interface ITicketService {

	public TicketResponse createNewTicket(TicketRequest request);

	public TicketResponse updateTicket(TicketRequest request, Long id);

	public List<TicketResponse> getAllTickets();

	public TicketResponse getTicketsById(Long id);

	public boolean deleteTicketsById(Long id);
}
