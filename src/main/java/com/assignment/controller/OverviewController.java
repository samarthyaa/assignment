package com.assignment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class OverviewController {

    @GetMapping("/")
    public String homePage() {
        return "home";
    }
}
