# Spring Web Application #

A sample REST application to demonstrate create, Get, Update and Delete operations.

### Includes ###

* REST APIs
* Test Cases
* [Swagger UI](http://localhost:8081/assignment/swagger-ui)
* [Swagger API Dcoumentation](http://localhost:8081/assignment/v2/api-docs)
* Application context : http://localhost:8081/assignment/

### Prerequisites? ###

* Java 8
* Maven

### How to run application ###

* Build `mvn clean install`
* Run `mvn spring-boot:run`
* Run with profile `mvn install spring-boot:run -Dspring-boot.run.profiles=dev`